from os import listdir
from os.path import isdir, isfile, basename, dirname, extsep, join as path_join, relpath
from useful import readfile_with_json_or_yaml_header

def filename_to_filedict(path, rootdir):
    full_path = path_join(rootdir, path)
    f = {'name': basename(path),
         'abspath':full_path,
         'original_path':path,
         'rootdir': rootdir,
        }
    if isdir(full_path):
        f['type'] = 'dir'
        f['path'] = relpath(full_path, rootdir)
    elif isfile(full_path):
        f['type'] = 'file'
        f['dir'] = relpath(dirname(full_path), rootdir)
        f['path'] = relpath(full_path, rootdir)
        f['extn'] = basename(path).split(extsep)[-1]
    else:
        f['type'] = 'other'
        f['dir'] = relpath(dirname(full_path), rootdir)
        f['path'] = relpath(full_path, rootdir)
        f['extn'] = basename(path).split(extsep)[-1]

    return f

def get_dir(where, root):
    # TODO: check if it does exist?
    deferred = []
    def _fulldir(f):
        return path_join(root, where, f)

    for f in [_fulldir(w) for w in listdir(path_join(root, where))]:
        if isdir(f):
            yield filename_to_filedict(f, root)
        else:
            deferred.append(f)

    deferred.sort()
    for f in deferred:
        yield filename_to_filedict(f, root)


