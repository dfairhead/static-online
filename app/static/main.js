//////////////////////////////////////////////
// main.js - Part of static-online
// (C) 2013 Daniel Fairhead
//////////////////////////////////////////////
// Requires jquery 1.9.x

// quick helpful function to get an element from HTML.
// we could use jQuery, I guess... TODO
function el(x){ return document.getElementById(x); }

////////////////////////////////////////////////////
// Message flashing stuff.
//
// Allows same interface for user if a message comes
// from the server, or the local javascript (for saving, etc).

function flash_message(text, time, classes) {
    classes = classes || '';
    time = time || 0;

    var msg = $('<div class="flashed '+classes+'">'+text+'</div>');
    $('#flashed_notices').prepend(msg);
    if (time != 0) {
        msg.delay(time).fadeOut(400, function(){$(this).remove();});
    }
}

$('#flashed_notices').delegate('.flashed', 'click', function(e){
    $(this).slideUp(300, function(){$(this).remove();})});

////////////////////////////////////////////////////////
// Saving edited stuff.
//
window.latest_edited_state = -1;
window.latest_saved_state = 0; // keep track of if the document has changed since last saved...

// POSTs save_data to current URL, and when saved, displays it.
function save_this_url(save_data, onsave){
    var edit_state_that_is_saved = window.latest_edited_state;
    onsave = onsave || function(){};

    // TODO: handle errors.
    $('#saving_status').html('Saving...');
    $.post(document.URL, save_data,
            function(data){
                window.latest_saved_state = edit_state_that_is_saved;
                flash_message('Saved.', 4000);
                $('#saving_status').html('');
                onsave();
            });
}

function go_or_save_and_go(where, save_data){
    if (window.latest_edited_state != window.latest_saved_state) {
        alert('edited:' + window.latest_edited_state + ': saved:' + window.latest_saved_state);
        if (window.confirm("Do you want to save changes?")) {
            // TODO: change to better popup boxes, probably jquery-ui as this is not friendly.
            // OK/Cancel for Do you want to save? stupid javascript..
            save_this_url(save_data, function(){window.location.href=where;});
            return 0;
        }
    }
    window.location.href = where;
}

/////////////////////////////////////////////////////////
// Search stuff...
/////////////////////////////////////////////////////////
$('#searchinput').autocomplete({
    source: "/search",
    minLength: 2,
    select: function(event, ui) {
        window.location.href = ui.item.value;
        }
    });
