from flask import render_template, url_for, request
from app import app
from os.path import join as path_join, relpath
from business import get_dir, filename_to_filedict
import views_files
from lib.pagestore import PageStore
import json

def dir_with_urls(path='.', base=app.config['BASEDIR']):
    def _add_url(f):
        if f['type'] == 'dir':
            f['url'] = url_for('view_dir', dirname=f['path'])
        elif f['type'] == 'file':
            f['url'] = url_for('view_file', filename=f['path'])
        else:
            f['url'] = '404.html'
        return f

    return [_add_url(f) for f in get_dir(path, base)]

@app.route('/')
@app.route('/index.html')
def index():
    return render_template('layout.html', files=dir_with_urls())

@app.route('/debug/')
def d():
    return str(app.config['BASEDIR'])

@app.route('/dirs/')
def _view_dir():
    return render_template('dir.html', files=dir_with_urls())
@app.route('/dirs/<path:dirname>')
def view_dir(dirname):
    return render_template('dir.html', files=dir_with_urls(dirname))

@app.route('/tags')
def tags_list():
    term = '' if 'term' not in request.args else request.args.get('term')

    def should_return(x):
        return x.startswith(term)

    try:
        with PageStore(app.config['PAGECACHE']) as c:
            return json.dumps(filter(should_return, c.all_tags()))
    except:
        return ''

@app.route('/search')
def search():
    if 'term' not in request.args:
        return ''

    def _cache_to_autocomplete(raw_item):
        item = json.loads(raw_item)
        return {'label': item.get('title', ''),
                'value': url_for('view_file',
                                 filename=relpath(item.get('filename', ''),
                                                  app.config['BASEDIR']))
                    }

    with PageStore(app.config['PAGECACHE']) as c:
        results = c.search(request.args['term']+'*')
        return json.dumps([_cache_to_autocomplete(x) for x in results])

#########################################################
#
# This should be split up somehow:
#
#########################################################

@app.route('/files/<path:filename>', methods=['GET','POST'])
def view_file(filename):
    filedict = filename_to_filedict(filename, app.config['BASEDIR'])
    filetype = filedict['extn']
    if filetype in views_files.HANDLERS:
        handler = views_files.HANDLERS[filetype](filedict)
        if request.method == 'GET':
            return handler.GET()
        elif request.method == 'POST':
            return handler.POST(request.form)
        else:
            return 'You should not be here.'
    else:
        return str(filedict['extn'])
        return "Sorry, I don't know what to do with that..."

    #with open(path_join(app.config['BASEDIR'],filename), 'r') as f:
    #    body = f.read()
    #return render_template('file.html', filename=filename, file_body=body)

@app.route('/reindex', methods=['POST'])
def reindex():
    ''' re-index all files in BASEDIR '''
    with PageStore(app.config['PAGECACHE']) as c:

        def _folder(args, location, files):
            for f in files:
                filename = relpath(path_join(location, f), app.config['BASEDIR'])


        c.purge(everything=True)

