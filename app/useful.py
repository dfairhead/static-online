# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import logging
import json
import yaml

class HeaderMetaDict(dict):
    # a dict. But it lets you keep track of *how* the data came into being,
    # if it comes from YAML, json, or whatever... useful it you want to write
    # the data back out in the same format (which makes users happy.)
    datatype = None

def readfile_splitat(filename, breakstring, ignore_if_starts_with=True):
    """ Read a text file, splitting at the first instance of $breakstring.
        useful for splitting a document into header (json or yaml) and content. """
    before_lines = []
    with open(filename,'r') as f:
        logging.debug('reading/splitting %s', filename)
        while True:
            line = f.readline()
            if line == '':
                # End of File... which means that there isn't any header block!
                return ('', ''.join(before_lines))
            elif line != breakstring:
                # Normal line, not the breakstring.
                before_lines.append(line)
            else:
                # we've found the breakstring.
                if ignore_if_starts_with and before_lines == []:
                    continue
                # return the header as a header, and the rest as the rest.
                return (''.join(before_lines), f.read())

    raise Exception('We should never have got here. This is BAD.')

def readfile_with_json_or_yaml_header(inputfile):
    header, content = readfile_splitat(inputfile, '---\n')
    context = HeaderMetaDict()
    if header:
        if header[:3] == '-j-':
            # json header.
            try:
                context = HeaderMetaDict(json.loads(header[3:]))
                context.datatype = 'json'
            except Exception as e:
                logging.error('Bad JSON header at the top of:' + inputfile)
                logging.error(str(e.message))
                exit(1)
        else:
            # maybe YAML header?
            try:
                context = HeaderMetaDict(yaml.safe_load(header))
                context.datatype = 'yaml'
            except Exception as e:
                # at this point, we have no real indication that there *is* a header.
                # either it's malformed YAML, or else it's not a header at all, so
                # we'll just dump it to the content, and hope it's not YAML.
                #       return (header + content, HeaderMetaDict())
                context.datatype = 'No JSON or YAML'
                content = header + content
    return (content, context)
