# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')


from flask import render_template
from app import app
from os.path import basename
from useful import readfile_with_json_or_yaml_header
import json
import yaml
from lib.pagestore import PageStore

class markdown():
    def __init__(self, filedict):
        self.filedict = filedict
        self.filename = filedict['abspath']

    def GET(self):
        contents, context = readfile_with_json_or_yaml_header(self.filename)

        # pop some bits out of the metadata which get displayed elsewhere
        pagetitle = context.pop('title', basename(self.filename))
        pagetags = ','.join(context.pop('tags',''))

        return render_template('file_types/markdown.html',
                               title=pagetitle,
                               contents=contents,
                               filename=basename(self.filename),
                               tags=pagetags,
                               full_filepath=self.filename,
                               path=self.filedict['dir'],
                               metadataformat=context.datatype,
                               json=json.dumps(context))

    def POST(self, values):
        if 'metadata' not in values \
        or 'metadataformat' not in values \
        or'content' not in values:
            return 'NOT GOOD!'

        metadata = json.loads(values['metadata'])

        # add tags to metadata:
        tags = values['tags'].split(',')
        metadata['tags'] = tags
        metadata['title'] = values['title']

        yaml_meta = yaml.safe_dump(metadata, default_flow_style=False)

        # write to the original file:
        with open(self.filename,'w') as f:
            # either metadata in JSON or YAML format:
            if values['metadataformat'] == 'json':
                f.write("-j-\n")
                f.write(json.dumps(metadata, indent=2))
            else:
                f.write('---\n')
                f.write(yaml_meta)

            # and now for the rest of the document:
            f.write("---\n")
            f.write(values['content'])

        # Now write to page cache:
        def save_to_pagestore(init=False):
            with PageStore(app.config['PAGECACHE']) as c:
                if init:
                    c.initialise()
                c.update(self.filename, \
                        values['content'], \
                        json.dumps({'title': metadata['title'], \
                                    'filename': self.filename}), \
                        '\n'.join([metadata['title'], yaml_meta, values['content']]), \
                        tags)

        try:
            save_to_pagestore()
        except:
            save_to_pagestore(True)

        # TODO: return something a bit more helpful...
        return render_template('debug.html', values=json.dumps(values, indent=True))

__extensions_to_filetypes = {
    'md': 'markdown',
    'js': 'javascript',
    'htm': 'html',
    }

def filetype(filename):
    extension = filename.split('.')[-1]

    if extension in __extensions_to_filetypes:
        return __extensions_to_filetypes[extension]
    else:
        return extension

class editable_text():
    def __init__(self, filedict):
        self.filedict = filedict
        self.filename = filedict['abspath']
    def GET(self):
        # TODO: error checking, and returning errors on file errors...
        with open(self.filename) as f:
            contents = f.read()
        return render_template('file_types/editable_text.html',
                               contents=contents,
                               filetype=filetype(self.filename),
                               filename=basename(self.filename),
                               full_filepath=self.filename)

    def POST(self, values):
        if 'content' in values:
            with open(self.filename,'w') as f:
                f.write(values['content'])
            return 'Saved!'


def image(filename):
    return render_template('file_types/image.html',
                           href=filename,
                           filename=basename(filename))

HANDLERS={
    'md'       : markdown,
    'markdown' : markdown,
    'txt'  : editable_text,
    'js'   : editable_text,
    'html' : editable_text,
    'png'  : image,
    'jpg'  : image,
    'jpeg' : image,
    'gif'  : image,

    }
