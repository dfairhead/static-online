-j-
{
  "section": "Contact", 
  "side-images": [
    {
      "filename": "bus-switcher-closeup.jpg", 
      "desc": "OB1 vision mixer closeup"
    }, 
    {
      "filename": "mouse-closeup.jpg"
    }, 
    {
      "filename": "bus-random.jpg"
    }
  ], 
  "template": "most", 
  "title": "Contact OMNIvision"
}---
Interested in a price quote, demo reel or conversation?
Feel free to contact us, we will be in touch.

--

 OMNIvision

 PO Box 27 
 
 Carlisle CA1 1HG 
 
 UK

 Phone <a href="tel://+441228815250">+44 (0) 1228 815 250</a>

<img class="black-on-transparent" src="/img/email-omnivision-at-om.png" width="137" height="35" alt="omnivision at om dot org"/>

--

 OM International Ltd
 Company No. 05649412 - Registered in England
 Charity No. 1112655 - VAT No. 932 3316 47

--

[Meet the team...](team.html)

[Join us...](join.html)

--

# Disclaimer

  Although we make every effort to control the contents of this website, we cannot take
  responsibility for opinions, information and links posted by external users.
  All contents and pictures are copyrighted material and must not be used without permission.

