---
meta-description: A page description
section: Event Services
side-images:
- desc: 9 meter blah blah
  filename: jib_blue_ts.jpg
- desc: jay at teenstreet
  filename: jay_event_handheld.jpg
- desc: Tim lining up one of out Sanyo projectors.
  filename: euan.jpg
tags:
- events
- production
- OB
template: most
title: Event Services
---
OMNIvision successfully covers a wide variety of live events across Western Europe, 
including major annual international youth festivals, corporate training seminars and 
church weekend retreats. Our equipment is versatile, our crew adaptable. Listening to 
you, we will quickly present a customisable production setup that is cost effective 
and appropriate to your needs.

## What we offer:

- <% accordion text="Live &amp; Recorded Video" %>
  + 6 Camera OB Truck with 3 M/E vision mixer and multi-track audio recording
  + 3 Camera OB Bus with 1 M/E vision mixer
  + 8 channel fly-away unit
  + 9 meter Jib: ABC Crane 120
  + Full crew
  + Graphics facilities
- <% accordion text="Image Magnification" %>
  + Wide range 16:9 and 4:3 screens
  + Professional large venue and HD projectors 
  + Multiple feeds from OB units
- <% accordion text="CD/DVD Authoring &amp; Duplication" %>
  + Duplication and label printing equipment
  + Discs available to order or on site
- <% accordion text="On Site Production" %>
  + Sony EX-1 HD cameras
  + Mobile edit suites with Final Cut Pro
- <% accordion text="Sound &amp; Light" %>
  + Speaker systems for audiences up to 1,000 people
  + Sound desks up to 40 channels
  + Outboard equipment, microphones, DIs etc.
  + PAR can and basic LED lighting systems with DMX lighting desks
- <% accordion text="Rigging &amp; Staging" %>
  + Truss
  + Black and white backdrops
  + 30 2x1m stage sections
- <% accordion text="Interpretation Equipment" %>
  + Up to 12 languages simultaneously
  + Wireless receivers
  + Booths for interpreters with microphones and headphone feeds

<% OMNItube video="ON-THE-ROAD" width="600" height="338" %>

## Our clients:

- <% accordion text="TeenStreet Germany" %>
  + Live IMAG, internet streaming & recorded video (Main Stage)
  + Interpretation (Main Stage)
  + Image Magnification (Main Stage)
  + Sound & Light (Second Stage)
  + On Site Production
  + <http://www.teenstreet.de/>
- <% accordion text="SummerFire" %>
  + Live & Recorded Video
  + Image Magnification
  + CD/DVD Authoring & Duplication
  + On Site Production
  + Rigging & Staging
  + www.summerfireconference.com
- <% accordion text="mission-net" %>
  + Live & Recorded Video (Main Stage)
  + Interpretation (Main Stage)
  + Image Magnification (Main Stage)
  + CD/DVD Authoring & Duplication (Main Stage)
  + Sound & Light (Second Stage)
  + On Site Production
- <% accordion text="Just10 North east" %>
  + Live & Recorded Video
  + Image Magnification
  + CD/DVD Authoring & Duplication

[More clients...](clients.html)
