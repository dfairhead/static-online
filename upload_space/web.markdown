-j-{
   "template" : "most",
      "title" : "Web stuff",
"side-images" : [
   { "filename" : "adam shooting mayan ruins at san andres.jpg",
         "desc" : "Adam Shooting Mayan Ruins in San Andres"},
   { "filename" : "jay shooting in the street.jpg",
         "desc" : "Jay on location, camera in hand."},
   { "filename" : "interviewing pastor luis.jpg",
         "desc" : "Interviewing a pastor in South America"}
    ]
}
---

We end up doing quite a bit of web stuff too, that's the way media is going these days.

## [OMNItube](http://www.omnitube.org):

[OMNItube](http://www.omnitube.org) is our media reservoir, loads of resources for missions.  Most of the videos are done by us, but there are also videos from others too.  We wrote and maintain the site.  Videos hosted there are very easy to embed on other sites, and cope pretty well on mobile and various desktop browsers.

# Other Projects:

These are some of our smaller web/software projects, which you may find useful, or interesting.
Then again, maybe you won't. These are open source, and availanble for your use.

## [OMNIcache](https://github.com/danthedeckie/OMNIcache)

A small simple basis for cacheing libraries in PHP.

## [OpenLP to ProPresenter 5 converter](https://github.com/danthedeckie/OpenLP-To-ProPresenter5-Converter#readme)

A fast simple script to convert your OpenLP song database into ProPresenter 5 format.

## [Stuff Management](https://github.com/danthedeckie/stuff-management)

Inventory management database software (work in progress...)

