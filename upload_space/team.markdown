---
tags:
- this
- is
- taglist
template: team
title: Meet the Team
---
![](/img/team/bryce.jpg)
Executive Producer, Director

[![](/img/team/philburt1.jpg)](http://www.facebook.com/v/450896654757)
Team Leader, Business Dev.

[![](/img/team/euanmacfarlane2.jpg)](http://www.facebook.com/v/386109899757)
Technical and Event Manager

[![](/img/team/robcook.jpg)](http://www.facebook.com/v/397080084757)
Events Literature Coordinator

![](/img/team/markrideout2.jpg)
Media Consultant

![](/img/team/beckygillen.jpg)
Bookshop Management

[![](/img/team/danielfairhead.jpg)](http://www.facebook.com/v/447193144757)
A/V Engineering and Web

[![](/img/team/mikaelalarraga.jpg)](http://www.facebook.com/v/449654254757)
Office Manager & Isaac's PA

![](/img/team/pedrosmit2.jpg)
Editor, Sound Technician

[![](/img/team/peteharris2.jpg)](http://www.facebook.com/v/1292875803892)
Vehicle and Construction Guru
