---
section: Video Production
side-images:
- desc: Adam Shooting Mayan Ruins in San Andres
  filename: adam shooting mayan ruins at san andres.jpg
- desc: Jay on location
  filename: jay shooting in the street.jpg
- desc: Interviewing a pastor in South America
  filename: interviewing pastor luis.jpg
template: most
title: Video Production
---
Using our worldwide network of partners, we can quickly capture the heartbeat of your ministry. Having worked in many countries around the world, OMNIvision makes it’s years of experience available to you. Your stories will be captured, your passions revealed. We can ensure that the video shot on location accurately portrays your ministry, captivating your target audience.

A fast turnaround of your video content into a final product will encompass your corporate image, showing a stylistic design suitable to the taste of your audience. OMNIvision is home to a number of editors working in facilities conducive to creativity.  The crew are trained in the use of Final Cut Pro and other relevant software tools. We can produce your materials or DVD orders in-house, quickly distributing them to you or your customers.

## What we offer:
- <% accordion text="Video Shoots" %>
  + On-location shoots worldwide
  + Multi-camera studio shoots at in-house filming facilities, includes lighting, sound and video
  + Shoots for interviews, personal profiles, advertisements, etc.
  + Filmed in Full High Definition
- <% accordion text="Audio Recording" %>
  + Fully equipped sound booth for voice over & narration
  + Background audio for on-location video shoots
  + Professional sound editing
- <% accordion text="Video Editing" %>
  + Professional editing of footage in fully equipped edit suites
  + Final Cut Studio (Final Cut Pro, Compressor, Motion, Soundtrack Pro, Final Cut Server)
  + Creative and innovative product by a talented and developing team of editors
- <% accordion text="CD/DVD Authoring &amp; Duplication" %>
  + Authoring software (DVD Studio Pro)
  + Professional DVD menus, layouts and labels
  + Duplication and printing of CDs and DVDs
  + Worldwide distribution

## Showreel:
<% OMNItube video="OMNIVISION-SHOWREEL" width=600 height=338 %>

