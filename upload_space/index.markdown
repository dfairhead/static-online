---
template: shell 
title: OMNIvision Video Production Studio
---
<div id="bg_textbox_a" class="bg_text">
    <span class="qo">&#8220;</span><span id="bg_text_a"></span>
</div>
<div id="bg_textbox_b" class="bg_text">
 <span class="qo2">&#8221;</span>
   <span id="bg_text_b"></span></div>

<div id="icons" class="clearfix">
<div id="eventicon" class="mainicon clearfix">
    <h2><a href="event-services.html">Events</a></h2>
    <p id="event_text" class="mainicondesc"> OMNIvision successfully covers a wide variety of live events across Western Europe, including major annual international youth festivals, corporate training seminars and church weekend retreats. Our equipment is versatile, our crew adaptable. Listening to you, we will quickly present a customisable production setup that is cost effective and appropriate to your needs.</p>
</div>

<div id="productionicon" class="mainicon">
    <h2><a href="video-production.html">Production</a></h2>
    <p id="production_text" class="mainicondesc">Using our worldwide network of partners, we can quickly capture the heartbeat of your ministry. Having worked in many countries around the world, OMNIvision makes it’s years of experience available to you. Your stories will be captured, your passions revealed. We can ensure that the video shot on location accurately portrays your ministry, captivating your target audience.  A fast turnaround of your video content into a final product will encompass your corporate image, showing a stylistic design suitable to the taste of your audience. OMNIvision is home to a number of editors working in facilities conducive to creativity.  The crew are trained in the use of Final Cut Pro and other relevant software tools. We can produce your materials or DVD orders in-house, quickly distributing them to you or your customers.
</p>
</div>

<div id="literatureicon" class="mainicon">
    <h2><a href="literature/">Literature</a></h2>
    <p id="literature_text" class="mainicondesc">OMNIvision sells and distributes quality Christian books in the UK for churches, events, mobile book buses and online sales. We provide thousands of titles from study Bibles, devotionals, and children's books, to Christian living and counselling, plus so much more.</p>
</div>

</div>
<script> var omnivision_front_page = true; </script>


