-j-{
"title" : "More about OMNIvision",
"template" : "most",
"side-images" : [
{ "filename" : "adam shooting mayan ruins at san andres.jpg",
      "desc" : "Adam Shooting Mayan Ruins in San Andres"},
{ "filename" : "jay shooting in the street.jpg",
      "desc" : "Jay on location, camera in hand."},
{ "filename" : "interviewing pastor luis.jpg",
      "desc" : "Interviewing a pastor in South America" }
      ]
}
---
OMNIvision is a production house specialising in live events, post production and 
international field coverage for Christian ministries and non-profit organisations. 
Our international team of specialists is driven to create excellent events and 
material that is used to transform lives and communities worldwide. In partnership
with you, our desire is to communicate what God is doing through your ministry. 
Providing affordable pricing and quality service, OMNIvision is ready to meet 
your production needs.

## History

Over a decade ago Ian Currey was working with OM's OMNI team to produce videos, 
media and AV for events.  The vision team then grew so rapidly that he consolidated 
it into the dedicated OMNIvision team in Carlisle, England to serve OM, Christian 
ministries and the global missions community.

<div style="text-align:center"><a href="http://www.om.org/" target="_blank"><img src="/img/om_logo_international_small.png" alt="OM International" /></a></div>

OMNIvision is a ministry of <a href="http://www.om.org" target="_blank">OM International</a>. 

'OMNI' is *OM*'s *N*ews and *I*nformation group.

