---
template: most
title: Join OMNIvision
---
OMNIvision is an international team of audio visual and multimedia specialists. 
We cover a wide range of events around Europe and produce videos all over the world.  
We have a full workload, but a lot of fun too.  Do you have what it takes to join us?

Relevant qualifications are desired but not a requirement to work with OMNIvision 
as we are a mixed team of professionals and enthusiastic learners. At OMNIvision 
you have the opportunity to acquire a wide spectrum of skills in a short time. 
There are many possibilities to work with us both short or long term as OMNIvision 
is a part of Operation Mobilisation, an organisation that works in more 
than 110 countries.

To get an idea of OMNIvision’s team life join our Facebook Page at:
[facebook.com/OMNIvision](http://www.facebook.com/omnivision)

or email:  omnivision-at-om-dot-org


## Please note:

All jobs within OMNIvision are done on a voluntary basis, i.e. you will not receive a salary.
Most people joining OM have to raise financial support to cover their living expenses,
usually through gifts from home churches and other supporters. If you have questions
about this just contact us or ask your local OM office.

