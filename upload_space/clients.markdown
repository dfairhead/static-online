---
tags:
- people
- teenstreet
template: clients
title: clients
---
[![OM International](/img/clients/client-om-international.jpg)](http://www.om.org)
Events, Production, etc.

[![BibelTV](/img/clients/client-bibeltv.jpg)](http://www.bibel-tv.de)
Production

[![](/img/clients/client-christianconvention.jpg)](http://www.christianconventions.org.uk/)
Annual Events

[![](/img/clients/client-citylinks.jpg)](http://www.citylinks.org.uk/)
Event

[![](/img/clients/client-crossrythms.jpg)](http://www.crossrhythms.co.uk/)
Event

[![](/img/clients/client-hkchurch.jpg)](http://www.hkchurch.org.uk/)
Events, AV installation

[![](/img/clients/client-hopeforjustice.jpg)](http://www.hopeforjustice.org.uk/)
Location shoot

[![](/img/clients/client-merceysidepolice.jpg)](http://www.merseyside.police.uk/)
Event

[![](/img/clients/client-message.jpg)](http://www.message.org.uk/)
Events

[![](/img/clients/client-missionnet.jpg)](http://www.mission-net.org/)
Bi-Annual Missions Event in Germany

[![](/img/clients/client-omships.jpg)](http://www.omships.org/)
Events, training, production, AV install

[![](/img/clients/client-philotrust.jpg)](http://www.philotrust.com/)
Events, Duplication

[![](/img/clients/client-rr-academy.jpg)](http://www.richardroseacademies.org/central/)
Events & Production

[![](/img/clients/client-salvationarmy.jpg)](http://www.salvationarmy.org.uk/)
Event

[![](/img/clients/client-soulsurvivor.jpg)](http://www.soulsurvivor.nl/)
Event

[![](/img/clients/client-summerfire.jpg)](http://www.summerfireconference.com/)
Annual Event in Ireland, Duplication, etc.

[![](/img/clients/client-teenstreet.jpg)](http://www.teenstreet.de/)
Annual Event in Germany.

[![](/img/clients/client-treehouse-media.jpg)](http://www.treehouse-media.co.uk/)
Event

[![](/img/clients/client-ucb.jpg)](http://www.ucb.co.uk/)
Video Productions

