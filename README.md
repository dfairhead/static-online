## static-online

A simple static site editor, for use with jekyll/marlinespike/hyde, or whatever.

## To get going:

run

    ./setup.sh

to get things set up, and then

    ./run.py

to run a basic dev server.  Full deployment options will be documented, once the software is at a fully usable
production state.  It's still very much WORK IN PROGRESS.


